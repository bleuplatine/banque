from comptesimple import Compte, CompteCourant
from personne import Personne

class Banque:
    ''' une banque qui gére des comptes bancaires,
    permettant d'ouvrir un compte, de prélever des frais bancaires
    et de calculer le solde de tous les comptes
    '''
    
    id = 10000

    def __init__(self):
        self.comptes = []
        # self.comptes_courant = []

    def __str__(self):
        return f"Montant total des comptes : {self.comptes}€"

    def __repr__(self):
        return f"banque {self.comptes}"

    def ouvrir_compte(self, titulaire, depot_initial):
        # ouvrir un compte bancaire et l'ajouter à la liste de compte de la banque
        Banque.id += 1
        self.id_compte = Banque.id
        compte = Compte(titulaire, depot_initial, self.id_compte)
        self.comptes.append(compte)
        return compte

    def ouvrir_compte_courant(self, titulaire, depot_initial):
        # ouvrir un compte courant et l'ajouter à la liste de compte courant de la banque
        Banque.id += 1
        self.id_compte = Banque.id
        compte_courant = CompteCourant(titulaire, depot_initial, self.id_compte)
        self.comptes.append(compte_courant)
        return compte_courant

    def calculer_solde(self):
        # calculer le montant total de tous les comptes bancaires
        return sum(compte.solde for compte in self.comptes)
        
    def prelever_frais(self, pourcent):
        # prélever un pourcentage de frais sur chaque compte et retourner le montant total
        for compte in self.comptes:
            compte.debiter(compte.solde * pourcent / 100)
            # if isinstance(compte, CompteCourant)
            #     somme_prel_compte = round((compte.solde * pourcent)/100, 2)
            #     somme_prelevee += somme_prel_compte
        # return somme_prelevee


if __name__ == "__main__":
    p1 = Personne("Seb")
    # Sebastien = Personne()
    p2 = Personne("Anne")
    HSBC = Banque()
    compte1 = HSBC.ouvrir_compte(p1, 1200.)
    courant2 = HSBC.ouvrir_compte_courant(p2, 1000)
    courant1 = HSBC.ouvrir_compte_courant(p1, 1500)
    compte2 = HSBC.ouvrir_compte(p2, 2000)
    HSBC.prelever_frais(10)
    # print(HSBC.calculer_solde())
    # print(HSBC.comptes)
    # print(courant1)
    # print(compte2)
    # print(courant2)
    courant1.crediter(125.45)
    compte1.debiter(332.30)
    compte1.crediter(123.25)
    courant1.debiter(235.40)
    compte1.editer_rlv_compte()
    courant1.editer_rlv_compte()
