from personne import Personne

class SoldeNegatif(Exception):
    # exception à lever dans le cas ou une opération entraine un solde négatif
    pass


class Compte:
    def __init__(self, titulaire, solde, id_compte):
        self.titulaire = titulaire
        self.__solde = solde
        self.id_compte = id_compte

    @property
    def solde(self):
        return self.__solde
            
    def __str__(self):
        return f"{self.titulaire} possède {self.solde}€ sur son compte bancaire {self.id_compte}."

    def __repr__(self):
        return f"Compte: titulaire({self.titulaire}) solde({self.solde}) id({self.id_compte})"
    

    def verifier_solde(self, argent):
        # vérifier que l'opération n'entraine pas un solde négatif > sinon lève une exception
        if self.solde < argent:
            raise SoldeNegatif("Pas assez d'argent sur le compte")

    def crediter(self, argent):
        # créditer le compte du montant argent
        self.__solde += argent

    def debiter(self, argent):
        # débiter le compte du montant argent aprés avoir vérifier que le solde est suffisant > sinon lève une exception
        self.verifier_solde(argent)
        self.__solde -= argent

    def editer_rlv_compte(self):
        print(self)

class CompteCourant(Compte):
    def __init__(self, titulaire, solde, id_compte):
        super().__init__(titulaire, solde, id_compte)
        self.historique = [("Dépot initial", solde)]
    
    def __str__(self):
        return f"{self.titulaire} possède {self.solde}€ sur son compte courant {self.id_compte}."

    def __repr__(self):
        return f"Compte courant: titulaire({self.titulaire}) solde({self.solde}) id({self.id_compte})"
    
    # enregistrer l'operation de debit dans une liste
    def add_debit(self, argent):
        self.historique.append(("Débit", argent))

    # enregistrer l'operation de credit dans une liste  
    def add_credit(self, argent):
        self.historique.append(("Crédit", argent))
    
    def verifier_solde(self, argent):
        # Vérifier que l'opération n'entraine pas un solde négatif > sinon lève une exception
        if self.solde < argent:
            raise SoldeNegatif("Pas assez d'argent sur le compte")

    def crediter(self, argent):
        # créditer le compte courant du montant argent
        super().crediter(argent)
        # ajouter l'opération de crédit à l'historique
        self.add_credit(argent)

    def debiter(self, argent):
        # débiter le compte courant du montant argent aprés avoir vérifier que le solde est suffisant > sinon lève une exception
        super().debiter(argent)
        # ajouter l'opération de débit à l'historique
        self.add_debit(-argent)

    def editer_rlv_compte(self):
        super().editer_rlv_compte()
        [print(i) for i in self.historique]
        # print(self.historique)
