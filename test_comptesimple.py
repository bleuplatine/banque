#!/bin/python3

import pytest

from banque import Banque
from comptesimple import Compte, CompteCourant
from personne import Personne

@pytest.fixture
def p1():
    return Personne("Seb")

@pytest.fixture
def p2():
    return Personne("Anne")

@pytest.fixture
def HSBC():
    return Banque()

@pytest.fixture
def c1(HSBC):
    return HSBC.ouvrir_compte(p1(), 1000) 

@pytest.fixture
def c2(HSBC):
    return HSBC.ouvrir_compte(p2(), 1500)

@pytest.fixture
def cpt1():
    return Compte("David", 1000, 10010)

@pytest.fixture
def cc1():
    return CompteCourant("Emilie", 2000, 10025)


def test_banque__init__(HSBC):
    assert HSBC.comptes == []

def test_init_personne(p1):
    assert p1.titulaire == "Seb"

def test_Compte__init__(cpt1):
    assert cpt1.titulaire == "David"
    assert cpt1.solde == 1000

def test_crediter_solde(cpt1):
    cpt1.crediter(200)
    assert cpt1.solde == 1200

def test_debiter_solde(cpt1):
    cpt1.debiter(500)
    assert cpt1.solde == 500

def test_CompteCourant_init(cc1):
    assert cc1.titulaire == "Emilie"
    assert cc1.solde == 2000

def test_crediter_solde_cc(cc1):
    cc1.crediter(200)
    assert cc1.solde == 2200


def test_debiter_solde_cc(cc1):
    cc1.debiter(500)
    assert cc1.solde == 1500

def test_ouvrir_C_et_CC(HSBC):
    c = HSBC.ouvrir_compte("Seb", 1000)
    cc = HSBC.ouvrir_compte_courant("Anne", 1500)
    assert c.solde == 1000
    assert c.titulaire == "Seb"
    assert c.id_compte == 10001
    assert cc.solde == 1500
    assert cc.titulaire == "Anne"
    assert cc.id_compte == 10002

def test_calculer_solde(HSBC):
    HSBC.ouvrir_compte("David", 1000)
    HSBC.ouvrir_compte("Anne", 1500)
    assert HSBC.calculer_solde() == 2500

def test_prelever_frais(HSBC):
    HSBC.ouvrir_compte("David", 1000)
    HSBC.ouvrir_compte("Anne", 1500)
    HSBC.prelever_frais(10)
    assert HSBC.calculer_solde() == 2250














